SELECT
	*
FROM
	(
		SELECT
			'1 week',
			u.id,
			u.nick,
			u.phone,
			u.`name`,
			u.created,
			t.cnt,
			t.siz,
			(t.cnt + t.siz * 0.01) AS soc,
			d.departName,
			(@rowNo := @rowNo + 1) AS ranking
		FROM
			IMDepart d,
			IMUser u,
			(
				SELECT
					fromId,
					sum(siz) siz,
					sum(cnt) cnt
				FROM
					(
						SELECT
							m.fromId,
							sum(LENGTH(m.content)) siz,
							count(0) cnt
						FROM
							IMMessage_0 m
						WHERE
							m.created > 1456132967
						GROUP BY
							m.fromId
						UNION ALL
							SELECT
								m.fromId,
								sum(LENGTH(m.content)) siz,
								count(0) cnt
							FROM
								IMMessage_1 m
							WHERE
								m.created > 1456132967
							GROUP BY
								m.fromId
							UNION ALL
								SELECT
									m.fromId,
									sum(LENGTH(m.content)) siz,
									count(0) cnt
								FROM
									IMMessage_2 m
								WHERE
									m.created > 1456132967
								GROUP BY
									m.fromId
								UNION ALL
									SELECT
										m.fromId,
										sum(LENGTH(m.content)) siz,
										count(0) cnt
									FROM
										IMMessage_3 m
									WHERE
										m.created > 1456132967
									GROUP BY
										m.fromId
									UNION ALL
										SELECT
											m.fromId,
											sum(LENGTH(m.content)) siz,
											count(0) cnt
										FROM
											IMMessage_4 m
										WHERE
											m.created > 1456132967
										GROUP BY
											m.fromId
										UNION ALL
											SELECT
												m.fromId,
												sum(LENGTH(m.content)) siz,
												count(0) cnt
											FROM
												IMMessage_5 m
											WHERE
												m.created > 1456132967
											GROUP BY
												m.fromId
											UNION ALL
												SELECT
													m.fromId,
													sum(LENGTH(m.content)) siz,
													count(0) cnt
												FROM
													IMMessage_6 m
												WHERE
													m.created > 1456132967
												GROUP BY
													m.fromId
												UNION ALL
													SELECT
														m.fromId,
														sum(LENGTH(m.content)) siz,
														count(0) cnt
													FROM
														IMMessage_7 m
													WHERE
														m.created > 1456132967 -- 1457514384   -- 1454132967
													GROUP BY
														m.fromId
					) AS t1
				GROUP BY
					fromid
			) AS t,
			(SELECT(@rowNo := 0)) b
		WHERE
			t.fromId = u.id
		AND d.id = u.departId
		ORDER BY
			soc DESC
	) t
