-- 傻逼需求
-- 统计时间周期 		1 +
-- 活跃排名 				1 +
-- 群组 						1 +
-- 是否新增					1 +
-- 建立时间 				1 +
-- 群主 						0
-- 登录手机号	  		0
-- 所属部门 				0
-- 群成员数 				1 +
-- 消息数（字节） 	1 +
-- 消息数（数目） 	1 +
SELECT
	*, (@rowNo := @rowNo + 1) AS ranking
FROM
	(
		SELECT
			*
		FROM
			(
				SELECT
					'1 week',
					g.id,
					g. NAME AS GroupName,
					FROM_UNIXTIME(g.created) AS CreationTime,
					(
						CASE
						WHEN g.`created` > 1461226305 THEN
							'aye'
						ELSE
							'nay'
						END
					) AS 'newGroup',
					g.userCnt AS PCU,
					gm.cnt AS MsgCount,
					gm.siz AS MsgSize,
					(
						g.userCnt * 0.1 + gm.cnt * 0.45 + gm.siz * 0.45
					) AS Points
				FROM
					imgroup g,
					(
						SELECT
							groupId,
							sum(siz) siz,
							sum(cnt) cnt
						FROM
							(
								SELECT
									m.groupId,
									sum(LENGTH(m.content)) siz,
									count(0) cnt
								FROM
									imgroupmessage_0 m
								WHERE
									m.created > 1454036867
								GROUP BY
									m.groupId
								UNION ALL
									SELECT
										m.groupId,
										sum(LENGTH(m.content)) siz,
										count(0) cnt
									FROM
										imgroupmessage_1 m
									WHERE
										m.created > 1454036867
									GROUP BY
										m.groupId
									UNION ALL
										SELECT
											m.groupId,
											sum(LENGTH(m.content)) siz,
											count(0) cnt
										FROM
											imgroupmessage_2 m
										WHERE
											m.created > 1454036867
										GROUP BY
											m.groupId
										UNION ALL
											SELECT
												m.groupId,
												sum(LENGTH(m.content)) siz,
												count(0) cnt
											FROM
												imgroupmessage_3 m
											WHERE
												m.created > 1454036867
											GROUP BY
												m.groupId
											UNION ALL
												SELECT
													m.groupId,
													sum(LENGTH(m.content)) siz,
													count(0) cnt
												FROM
													imgroupmessage_4 m
												WHERE
													m.created > 1454036867
												GROUP BY
													m.groupId
												UNION ALL
													SELECT
														m.groupId,
														sum(LENGTH(m.content)) siz,
														count(0) cnt
													FROM
														imgroupmessage_5 m
													WHERE
														m.created > 1454036867
													GROUP BY
														m.groupId
													UNION ALL
														SELECT
															m.groupId,
															sum(LENGTH(m.content)) siz,
															count(0) cnt
														FROM
															imgroupmessage_6 m
														WHERE
															m.created > 1454036867
														GROUP BY
															m.groupId
														UNION ALL
															SELECT
																m.groupId,
																sum(LENGTH(m.content)) siz,
																count(0) cnt
															FROM
																imgroupmessage_7 m
															WHERE
																m.created > 1454036867 -- 1457514384   -- 1454132967
															GROUP BY
																m.groupId
							) AS t
						GROUP BY
							groupid
					) AS gm,
					(SELECT(@rowNo := 0)) b
				WHERE
					gm.groupId = g.id -- AND g.id = u.departId
			) p
		ORDER BY
			Points DESC
	) r