SELECT
	t.*, (@rowNo := @rowNo + 1) AS ranking
FROM
	(
		SELECT
			IMUser.id,
			IMUser.nick,
			IMUser.phone,
			IMUser.`name`,
			IMUser.created,
			messageAll.cnt,
			messageAll.siz,
			(
				messageAll.cnt + messageAll.siz * 0.01
			) AS soc,
			IMDepart.departName
		FROM
			(
				SELECT
					m.fromId,
					sum(LENGTH(m.content)) siz,
					count(0) cnt
				FROM
					IMMessage_0 m
				WHERE
					m.created > 1456132967
				GROUP BY
					m.fromId
				UNION ALL
					SELECT
						m.fromId,
						sum(LENGTH(m.content)) siz,
						count(0) cnt
					FROM
						IMMessage_1 m
					WHERE
						m.created > 1456132967
					GROUP BY
						m.fromId
					UNION ALL
						SELECT
							m.fromId,
							sum(LENGTH(m.content)) siz,
							count(0) cnt
						FROM
							IMMessage_2 m
						WHERE
							m.created > 1456132967
						GROUP BY
							m.fromId
						UNION ALL
							SELECT
								m.fromId,
								sum(LENGTH(m.content)) siz,
								count(0) cnt
							FROM
								IMMessage_3 m
							WHERE
								m.created > 1456132967
							GROUP BY
								m.fromId
							UNION ALL
								SELECT
									m.fromId,
									sum(LENGTH(m.content)) siz,
									count(0) cnt
								FROM
									IMMessage_4 m
								WHERE
									m.created > 1456132967
								GROUP BY
									m.fromId
								UNION ALL
									SELECT
										m.fromId,
										sum(LENGTH(m.content)) siz,
										count(0) cnt
									FROM
										IMMessage_5 m
									WHERE
										m.created > 1456132967
									GROUP BY
										m.fromId
									UNION ALL
										SELECT
											m.fromId,
											sum(LENGTH(m.content)) siz,
											count(0) cnt
										FROM
											IMMessage_6 m
										WHERE
											m.created > 1456132967
										GROUP BY
											m.fromId
										UNION ALL
											SELECT
												m.fromId,
												sum(LENGTH(m.content)) siz,
												count(0) cnt
											FROM
												IMMessage_7 m
											WHERE
												m.created > 1456132967 -- 1457514384   -- 1454132967
											GROUP BY
												m.fromId
			) messageAll
		LEFT JOIN IMUser ON messageAll.fromid = imuser.id
		LEFT JOIN IMDepart ON imuser.departId = IMDepart.id
		GROUP BY
			imuser.id
	) AS t,
	(SELECT(@rowNo := 0)) b
ORDER BY
	t.soc DESC