set @name = '柳河',@begin = 1464772772,@end = 1467343380;
  
SELECT
	nick,
	msg.time,
	msg.content
FROM
	IMUser,
	(
		SELECT
			fromId,
			updated AS time,
			content
		FROM
			IMMessage_0
		WHERE
			updated < @end
		AND updated > @begin
		GROUP BY
			updated
		UNION ALL
			SELECT
				fromId,
				updated AS time,
				content
			FROM
				IMMessage_1
			WHERE
				updated < @end
			AND updated > @begin
			GROUP BY
				updated
			UNION ALL
				SELECT
					fromId,
					updated AS time,
					content
				FROM
					IMMessage_2
				WHERE
					updated < @end
				AND updated > @begin
				GROUP BY
					updated
				UNION ALL
					SELECT
						fromId,
						updated AS time,
						content
					FROM
						IMMessage_3
					WHERE
						updated < @end
					AND updated > @begin
					GROUP BY
						updated
					UNION ALL
						SELECT
							fromId,
							updated AS time,
							content
						FROM
							IMMessage_4
						WHERE
							updated < @end
						AND updated > @begin
						GROUP BY
							updated
						UNION ALL
							SELECT
								fromId,
								updated AS time,
								content
							FROM
								IMMessage_5
							WHERE
								updated < @end
							AND updated > @begin
							GROUP BY
								updated
							UNION ALL
								SELECT
									fromId,
									updated AS time,
									content
								FROM
									IMMessage_6
								WHERE
									updated < @end
								AND updated > @begin
								GROUP BY
									updated
								UNION ALL
									SELECT
										fromId,
										updated AS time,
										content
									FROM
										IMMessage_7
									WHERE
										updated < @end
									AND updated > @begin
									GROUP BY
										updated
	) msg
WHERE
	IMUser.id = msg.fromId
AND IMUser.nick = @name