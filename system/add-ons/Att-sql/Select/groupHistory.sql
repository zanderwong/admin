-- EXAMPLE: USER: 解伟
SET @uname = '解伟',
 @gname = '格力电器周核心质量问题分析改善群' ,@BEGIN = 1467331200 ,@END = 1468281600;

SELECT
	g.id AS gID,
	g.`name` AS gName,
	gm.userId AS uID,
	u.nick AS uName,
	FROM_UNIXTIME(gm.updated) AS time,
	gm.content
FROM
	IMUser u,
	IMGroup g,
	(
		SELECT
			groupId AS id,
			userId,
			updated,
			content
		FROM
			IMGroupMessage_0
		WHERE
			updated > @BEGIN
		AND updated < @END
		UNION ALL
			SELECT
				groupId AS id,
				userId,
				updated,
				content
			FROM
				IMGroupMessage_1
			WHERE
				updated > @BEGIN
			AND updated < @END
			UNION ALL
				SELECT
					groupId AS id,
					userId,
					updated,
					content
				FROM
					IMGroupMessage_2
				WHERE
					updated > @BEGIN
				AND updated < @END
				UNION ALL
					SELECT
						groupId AS id,
						userId,
						updated,
						content
					FROM
						IMGroupMessage_3
					WHERE
						updated > @BEGIN
					AND updated < @END
					UNION ALL
						SELECT
							groupId AS id,
							userId,
							updated,
							content
						FROM
							IMGroupMessage_4
						WHERE
							updated > @BEGIN
						AND updated < @END
						UNION ALL
							SELECT
								groupId AS id,
								userId,
								updated,
								content
							FROM
								IMGroupMessage_5
							WHERE
								updated > @BEGIN
							AND updated < @END
							UNION ALL
								SELECT
									groupId AS id,
									userId,
									updated,
									content
								FROM
									IMGroupMessage_6
								WHERE
									updated > @BEGIN
								AND updated < @END
								UNION ALL
									SELECT
										groupId AS id,
										userId,
										updated,
										content
									FROM
										IMGroupMessage_7
									WHERE
										updated > @BEGIN
									AND updated < @END
	) gm
WHERE
	gm.id = g.id
AND gm.userId = u.id
AND g.`name` = @gname
AND u.nick LIKE CONCAT('%', @uname)