<?php
include_once(APPPATH."core/TT_Model.php");
class Sequence_model extends TT_Model{
	function __construct(){
		parent::__construct();
		$this->table_name = 'IMSequence';
	}

	public function getNext($key, $defaultValue)
	{    
		$last = $defaultValue  ;
		
		$row = $this->getOne(array('key'=>$key) ,'value');
		 
		if (count($row) ==1 ) 
		{
			$last = $row['value']  ;
			$last = $last +1 ;
			$this->db->update($this->table_name, array('key'=>$key,'value'=> $last )); 
		}
		else
		{
			$last = $last +1 ;
			$this->db->insert($this->table_name,  array('key'=>$key,'value'=> $last )); 
		}
			    
		 
		return $last;
	}
}