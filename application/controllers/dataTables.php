<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH . "core/TT_Controller.php");

class DataTables extends TT_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
    }

    public function index() {
        $this->load->view('base/header');
        $this->load->view('base/dataTables');
        $this->load->view('base/footer');
    }


}