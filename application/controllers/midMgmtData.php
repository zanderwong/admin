<?php
/**
 * Created by PhpStorm.
 * User: Zander Wong
 * Date: 5/13/2016
 * Time: 10:51 AM
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH . "core/TT_Controller.php");

class MidMgmtData extends TT_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('user_model');
        $this->load->model('depart_model');
    }

    public function index() {
        $this->config->site_url();
        $this->load->view('base/header');
        $this->load->view('base/midMgmtData');
        $this->load->view('base/footer');
    }

    public function all() {
        $start = $this->input->get('start');
        if (!$start) {
            $start = 0;
        }

        $boat = $this->input->get('boat');
        if (!$boat) {
            $boat = "all";
        }

        switch ($boat) {
            case "weekly":
                $timeline = time() - 7 * 24 * 60 * 60;
                break;
            case "monthly":
                $timeline = time() - 30 * 24 * 60 * 60;
                break;
            default:
                $timeline = 140000000;
        }
//        set init timeline : 1400000000
        $sql = <<<EOT
SELECT
	*
FROM
	(
		SELECT
			u.id,
			u.nick,
			u.phone,
			FROM_UNIXTIME(u.created) AS signup,
			(
				CASE
				WHEN u.`created` > $timeline THEN
					'√'
				ELSE
					''
				END
			) AS 'newGuy',
			userMsgCnt.cnt,
			userMsgCnt.siz,
			(userMsgCnt.cnt) AS soc,
			d.departName
		FROM
			(
				SELECT
					fromId,
					sum(siz) siz,
					sum(cnt) cnt
				FROM
					(
						SELECT
							m.fromId,
							sum(LENGTH(m.content)) siz,
							count(0) cnt
						FROM
							IMMessage_0 m
						WHERE
							m.created > $timeline
						GROUP BY
							m.fromId
						UNION ALL
							SELECT
								m.fromId,
								sum(LENGTH(m.content)) siz,
								count(0) cnt
							FROM
								IMMessage_1 m
							WHERE
								m.created > $timeline
							GROUP BY
								m.fromId
							UNION ALL
								SELECT
									m.fromId,
									sum(LENGTH(m.content)) siz,
									count(0) cnt
								FROM
									IMMessage_2 m
								WHERE
									m.created > $timeline
								GROUP BY
									m.fromId
								UNION ALL
									SELECT
										m.fromId,
										sum(LENGTH(m.content)) siz,
										count(0) cnt
									FROM
										IMMessage_3 m
									WHERE
										m.created > $timeline
									GROUP BY
										m.fromId
									UNION ALL
										SELECT
											m.fromId,
											sum(LENGTH(m.content)) siz,
											count(0) cnt
										FROM
											IMMessage_4 m
										WHERE
											m.created > $timeline
										GROUP BY
											m.fromId
										UNION ALL
											SELECT
												m.fromId,
												sum(LENGTH(m.content)) siz,
												count(0) cnt
											FROM
												IMMessage_5 m
											WHERE
												m.created > $timeline
											GROUP BY
												m.fromId
											UNION ALL
												SELECT
													m.fromId,
													sum(LENGTH(m.content)) siz,
													count(0) cnt
												FROM
													IMMessage_6 m
												WHERE
													m.created > $timeline
												GROUP BY
													m.fromId
												UNION ALL
													SELECT
														m.fromId,
														sum(LENGTH(m.content)) siz,
														count(0) cnt
													FROM
														IMMessage_7 m
													WHERE
														m.created > $timeline
													GROUP BY
														m.fromId
													UNION ALL
														SELECT
															m.fromId,
															sum(LENGTH(m.content)) siz,
															count(0) cnt
														FROM
															IMMessage_7 m
														WHERE
															m.created > $timeline
														GROUP BY
															m.fromId
														UNION ALL
															SELECT
																m.fromId,
																sum(LENGTH(m.content)) siz,
																count(0) cnt
															FROM
																IMMessage_7 m
															WHERE
																m.created > $timeline
															GROUP BY
																m.fromId
					) AS t1
				GROUP BY
					fromid
			) AS userMsgCnt
		LEFT JOIN IMUser u ON userMsgCnt.fromId = u.id
		LEFT JOIN IMDepart d ON d.id = u.departId
	) t
JOIN (
	SELECT
		IMGroupMember.userId,
		count(0) AS gNum,
		IMGroupMember.groupId AS GroupID
	FROM
		IMGroupMember
	WHERE
		IMGroupMember.GroupID = 1000002
	    OR IMGroupMember.GroupID = 1000003
	    OR IMGroupMember.GroupID = 1500002
	    OR IMGroupMember.GroupID = 1500003
	    OR IMGroupMember.GroupID = 1500015
	GROUP BY
		IMGroupMember.userId
) g ON t.id = g.userId
ORDER BY
	soc DESC
EOT;
        $res = $this->user_model->getQuery($sql);
        $result = array(
            'res' => $res,
            'page' => $start,
        );
        echo json_encode($result);
        return;
    }
}

