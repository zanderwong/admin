<?php
/**
 * Created by PhpStorm.
 * User: wander
 * Date: 7/9/2016
 * Time: 9:22 PM
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH . "core/TT_Controller.php");

class GroupHistory extends TT_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('user_model');
        $this->load->model('group_model');
    }

    public function index() {
        $this->config->site_url();
        $this->load->view('base/header');
        $this->load->view('base/groupHistory');
        $this->load->view('base/footer');
    }

    public function all() {
        $uname = $this->input->get('uname');
        $gname = $this->input->get('gname');
        $begin = $this->input->get('checkin') / 1000;
        $end = $this->input->get('checkout') / 1000;

//        SET $uname = '解伟',
// $gname = '格力电器周核心质量问题分析改善群' ,$begin = 1420070400 ,$end = 1468281600;
        $sql = <<<EOT
SELECT
	g.id AS gID,
	g.`name` AS gName,
	gm.userId AS uID,
	u.nick AS uName,
	FROM_UNIXTIME(gm.updated) AS time,
	gm.content
FROM
	IMUser u,
	IMGroup g,
	(
		SELECT
			groupId AS id,
			userId,
			updated,
			content
		FROM
			IMGroupMessage_0
		WHERE
			updated > $begin
		AND updated < $end
		UNION ALL
			SELECT
				groupId AS id,
				userId,
				updated,
				content
			FROM
				IMGroupMessage_1
			WHERE
				updated > $begin
			AND updated < $end
			UNION ALL
				SELECT
					groupId AS id,
					userId,
					updated,
					content
				FROM
					IMGroupMessage_2
				WHERE
					updated > $begin
				AND updated < $end
				UNION ALL
					SELECT
						groupId AS id,
						userId,
						updated,
						content
					FROM
						IMGroupMessage_3
					WHERE
						updated > $begin
					AND updated < $end
					UNION ALL
						SELECT
							groupId AS id,
							userId,
							updated,
							content
						FROM
							IMGroupMessage_4
						WHERE
							updated > $begin
						AND updated < $end
						UNION ALL
							SELECT
								groupId AS id,
								userId,
								updated,
								content
							FROM
								IMGroupMessage_5
							WHERE
								updated > $begin
							AND updated < $end
							UNION ALL
								SELECT
									groupId AS id,
									userId,
									updated,
									content
								FROM
									IMGroupMessage_6
								WHERE
									updated > $begin
								AND updated < $end
								UNION ALL
									SELECT
										groupId AS id,
										userId,
										updated,
										content
									FROM
										IMGroupMessage_7
									WHERE
										updated > $begin
									AND updated < $end
	) gm
WHERE
	gm.id = g.id
AND gm.userId = u.id
AND g.`name` = '$gname'
AND u.nick LIKE CONCAT('%', '$uname')
EOT;

        $res = $this->group_model->getQuery($sql);
        $result = array(
            'res' => $res,
            'sql' => $sql,
        );
        echo json_encode($result);

    }
}
