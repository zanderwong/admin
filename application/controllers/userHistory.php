<?php
/**
 * Created by PhpStorm.
 * User: Zander Wong
 * Date: 2016/7/4
 * Time: 15:19
 */

if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH . "core/TT_Controller.php");

class UserHistory extends TT_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('user_model');
        $this->load->model('depart_model');
    }

    public function index() {
        $this->config->site_url();
        $this->load->view('base/header');
        $this->load->view('base/userHistory');
        $this->load->view('base/footer');
    }

    public function all() {

        // $start = $this->input->get('start');
        $name = $this->input->get('name');
        $begin = $this->input->get('checkin');
        $end = $this->input->get('checkout');

        $sql = <<<EOT
            SELECT
                id,
                nick,
                FROM_UNIXTIME(msg.time) as time,
                msg.content
            FROM
                IMUser,
                (
                    SELECT
                        fromId,
                        updated AS time,
                        content
                    FROM
                        IMMessage_0
                    WHERE
                        updated < $end
                    AND updated > $begin
                    GROUP BY
                        updated
                    UNION ALL
                    SELECT
                        fromId,
                        updated AS time,
                        content
                    FROM
                        IMMessage_1
                    WHERE
                        updated < $end
                    AND updated > $begin
                    GROUP BY
                        updated
                    UNION ALL
                    SELECT
                        fromId,
                        updated AS time,
                        content
                    FROM
                        IMMessage_2 
                    WHERE
                        updated < $end
                    AND updated > $begin
                    GROUP BY
                        updated
                    UNION ALL
                    SELECT
                        fromId,
                        updated AS time,
                        content
                    FROM
                        IMMessage_3
                    WHERE
                        updated < $end
                    AND updated > $begin
                    GROUP BY
                        updated
                    UNION ALL
                    SELECT
                        fromId,
                        updated AS time,
                        content
                    FROM
                        IMMessage_4
                    WHERE
                        updated < $end
                    AND updated > $begin
                    GROUP BY
                        updated
                    UNION ALL
                    SELECT
                        fromId,
                        updated AS time,
                        content
                    FROM
                        IMMessage_5
                    WHERE
                        updated < $end
                    AND updated > $begin
                    GROUP BY
                        updated
                    UNION ALL
                    SELECT
                        fromId,
                        updated AS time,
                        content
                    FROM
                        IMMessage_6
                    WHERE
                        updated < $end
                    AND updated > $begin
                    GROUP BY
                        updated
                    UNION ALL
                    SELECT
                        fromId,
                        updated AS time,
                        content
                    FROM
                        IMMessage_7
                    WHERE
                        updated < $end
                    AND updated > $begin
                    GROUP BY
                        updated
                ) msg
            WHERE
                IMUser.id = msg.fromId
            AND IMUser.nick = '$name'
EOT;

        $res = $this->user_model->getQuery($sql);
        $result = array(
            'res' => $res,
            'sql' => $sql,
            // 'page' => $start,
        );
        echo json_encode($result);
    }
}
