<?php
/**
 *   Created by PhpStorm.
 *   User: Zander Wong
 *   Date: 2016/4/26
 *   Time: 14:10
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once(APPPATH . "core/TT_Controller.php");

class GroupData extends TT_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('user_model');
        $this->load->model('group_model');
    }

    public function index() {
        $this->config->site_url();
        $this->load->view('base/header');
        $this->load->view('base/groupData');
        $this->load->view('base/footer');
    }

    public function all() {
        $start = $this->input->get('start');
        if (!$start) {
            $start = 0;
        }
//        TODO : Add Cycle select function 
        $boat = $this->input->get('boat');
        if (!$boat) {
            $boat = "all";
        }

        switch ($boat) {
            case "weekly":
                $timeline = time() - 7 * 24 * 60 * 60;
                break;
            case "monthly":
                $timeline = time() - 30 * 24 * 60 * 60;
                break;
            case "all":
                $timeline = 140000000;
                break;
            default:
                $timeline = 140000000;
        }
//        set init timeline : 1400000000

//        TODO : Add RowNumber function in the SQL
//		    TODO :convert to format Date form UNIXTIME
//              TODO :check to see if the group created in this week
//                  TODO :select how many users in the group
        $sql = <<<EOT
SELECT
	*, (@rowNo := @rowNo + 1) AS ranking
FROM
	(
		SELECT
			*
		FROM
			(
				SELECT
					'1 week',
					g.id,
					g. NAME AS GroupName,
					u.nick as Creator,
					FROM_UNIXTIME(g.created) AS CreationTime,
					(
						CASE
						WHEN g.`created` > $timeline THEN
							'√'
						ELSE
							''
						END
					) AS 'newGroup',
					g.userCnt AS PCU,
					gm.cnt AS MsgCount,
					gm.siz AS MsgSize,
					(
						g.userCnt * 0.1 + gm.cnt * 0.45 + gm.siz * 0.45
					) AS Points
				FROM
					IMUser u,
					IMGroup g,
					(
						SELECT
							groupId,
							sum(siz) siz,
							sum(cnt) cnt
						FROM
							(
								SELECT
									m.groupId,
									sum(LENGTH(m.content)) siz,
									count(0) cnt
								FROM
									IMGroupMessage_0 m
								WHERE
									m.created > $timeline
								GROUP BY
									m.groupId
								UNION ALL
									SELECT
										m.groupId,
										sum(LENGTH(m.content)) siz,
										count(0) cnt
									FROM
										IMGroupMessage_1 m
									WHERE
										m.created > $timeline
									GROUP BY
										m.groupId
									UNION ALL
										SELECT
											m.groupId,
											sum(LENGTH(m.content)) siz,
											count(0) cnt
										FROM
											IMGroupMessage_2 m
										WHERE
											m.created > $timeline
										GROUP BY
											m.groupId
										UNION ALL
											SELECT
												m.groupId,
												sum(LENGTH(m.content)) siz,
												count(0) cnt
											FROM
												IMGroupMessage_3 m
											WHERE
												m.created > $timeline
											GROUP BY
												m.groupId
											UNION ALL
												SELECT
													m.groupId,
													sum(LENGTH(m.content)) siz,
													count(0) cnt
												FROM
													IMGroupMessage_4 m
												WHERE
													m.created > $timeline
												GROUP BY
													m.groupId
												UNION ALL
													SELECT
														m.groupId,
														sum(LENGTH(m.content)) siz,
														count(0) cnt
													FROM
														IMGroupMessage_5 m
													WHERE
														m.created > $timeline
													GROUP BY
														m.groupId
													UNION ALL
														SELECT
															m.groupId,
															sum(LENGTH(m.content)) siz,
															count(0) cnt
														FROM
															IMGroupMessage_6 m
														WHERE
															m.created > $timeline
														GROUP BY
															m.groupId
														UNION ALL
															SELECT
																m.groupId,
																sum(LENGTH(m.content)) siz,
																count(0) cnt
															FROM
																IMGroupMessage_7 m
															WHERE
																m.created > $timeline 
															GROUP BY
																m.groupId
							) AS t
						GROUP BY
							groupid
					) AS gm,
					(SELECT(@rowNo := 0)) b
				WHERE
					gm.groupId = g.id
				AND u.id = g.creator
			) p
		ORDER BY
			Points DESC
	) r
EOT;
        $gres = $this->group_model->getQuery($sql);
        $result = array(
            'gres' => $gres,
            'page' => $start,
        );
        echo json_encode($result);
        return;
    }
}
