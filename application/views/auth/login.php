<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>GreeLink后台管理系统 | 登陆</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="../../ui/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="../../ui/ajax/libs/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../ui/ajax/libs/form-elements/form-elements.css">
    <link rel="stylesheet" href="../../ui/ajax/libs/style/style.css">
    <link rel="icon" href="../../ui/ico/favicon.ico" type="image/x-icon" />

    <!-- <link href="../../ui/css/main.css" rel="stylesheet" type="text/css"/> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>

    <!--BEGIN login_box-->
    <div class="top-content">
        
        <div class="inner-bg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 text">
                        <h1 style="font-family:Microsoft YaHei; font-weight:500"><strong>GreeLink</strong> 后台管理系统</h1>
                        <div class="description">
                            <p>
                                
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 form-box">
                        <div class="form-top">
                            <div class="form-top-left">
                                <h3>Login to the management site</h3>
                                <p>Enter your username and password to log on:</p>
                            </div>
                            <div class="form-top-right">
                                <i class="fa fa-key"></i>
                            </div>
                        </div>
                        <div class="form-bottom">
                            <form action="/auth/login" method="post" class="login-form">                           
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Username</label>
                                    <input type="text" name="admin" placeholder="用户名" class="form-control admin" id="form-username"/>
                                    <!-- <input type="text" name="admin" class="form-control admin" placeholder="用户名"/> -->
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="form-password">Password</label>
                                    <input type="password" name="password" placeholder="密码" class="form-control password" id="form-password"/>
                                    <!-- <input type="password" name="password" class="form-control password" placeholder="密码"/> -->
                                </div>
                                <!-- <button type="submit" class="btn">登 陆</button> -->
                                <button type="submit" class="btn bg-olive btn-block login_btn" style="font-size:20px; font-family:Microsoft YaHei; font-weight:500">登   陆</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <!--END login_box-->
    <!-- <script src="../../ui/plugins/jQuery/jquery-1.11.1.min.js"></script> -->

    <script src="../../ui/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- <script src="../../ui/js/bootstrap.min.js"></script> -->
    <script src="../../ui/bootstrap/js/bootstrap.min.js"></script>

    <script src="../../ui/plugins/backstretch/jquery.backstretch.min.js"></script>
    <script src="../../ui/dist/js/scripts.js"></script>

    <script type="text/javascript">
        $(function () {
            $("form").submit(function () {
                $.post('../auth/login', {
                    admin: $(".admin").val(),
                    password: $(".password").val(),
                    submit: 1
                }, function (data) {
                    data = $.trim(data);
                    if (data == 'right') {
                        $(".login_mes").text('登陆成功,正在跳转...').removeClass("hide");
                        window.location.href = "./";
                    } else {
                        $(".login_mes").text('密码错误').removeClass("hide");
                    }
                });
                return false;
            })
        })
    </script>
</body>
</html>