<!--/**-->
<!--* Created by PhpStorm.-->
<!--* User: Zander Wong-->
<!--* Date: 2016/7/8-->
<!--* Time: 11:15-->
<!--*/-->
<aside class="right-side">
    <!-- content header -->
    <section class="content-header">
        <h1>用户历史
            <small>欢迎来到GreeLink</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="./home"><i class="fa fa-dashboard"></i> 首页 </a></li>
            <li><a href="#">历史查询</a></li>
            <li class="active">用户历史</li>
        </ol>
    </section>
    <!-- main content -->
    <section class="content">
        <!-- DataTables -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- box-header -->
                    <div class="box-header" style="height: auto">
                        <h3 class="box-title">Hover Data Table</h3>
                        <div class="col-xs-12" style="height:60px">
                            <div class="box-tools">
                                <!-- TODO:add username input field here -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label> </label>
                                        <div class="input-group" style="width: 50%">
                                            <div class="input-group-addon">
                                                <i class="fa fa-user"></i>
                                            </div>
                                            <input type="text" class="form-control" placeholder="User Name" id="uname">
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <!-- TODO:add datePicker field here -->
                                    <!-- Date range -->
                                    <div class="form-group">
                                        <label> </label>
                                        <div class="input-group" style="width: 80%">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" placeholder="Date Range" id="reservation">
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <!-- TODO:add submit button here -->
                                    <div class="form-group">
                                        <label> </label>
                                        <div class="btn-group" role="group"  style="padding-top:20px">
                                            <button type="button" class="btn btn-primary" onclick="argsPass()">显示内容</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <!-- box-body -->
                    <div class="box-body">
                        <table id="uhistory" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>用户姓名</th>
                                    <th>会话时间</th>
                                    <th>会话历史</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- TODO: userData inject -->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>用户姓名</th>
                                    <th>会话时间</th>
                                    <th>会话历史</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- DataTables -->
    <script src="../ui/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../ui/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- DateRangePicker -->
    <script src="ui/ajax/moment.js/moment.min.js"></script>
    <script src="ui/plugins/daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript">
    var checkin;
    var checkout;
    var queryFunc = {
        compiledTpl: null,
        getUserHisData: function(page) {


            var uname = $("#uname").val();
            // var checkin = $("#dpd1").val();
            // var checkout = $("#dpd2").val();

            // checkin = (new Date(checkin)).getTime();
            // checkout = (new Date(checkout)).getTime();
            console.log(uname);
            console.log(checkin);
             console.log(checkout);


            $.ajax({
                url: "./userHistory/all",
                type: "get",
                data: {
                    start: page,
                    "name": uname,
                    "checkin": checkin,
                    "checkout": checkout
                },
                success: function(data) {
                    queryFunc.tpl();
                    var _tpl = queryFunc.compiledTpl.render(data);
                    console.log(_tpl);
                    $("tbody").html(_tpl);
                    $('#uhistory').DataTable({
                      "paging": true,
                      "lengthChange": false,
                      "searching": false,
                      "ordering": true,
                      "info": false,
                      "autoWidth": false,
                      "retrieve":true,
                      "destroy":true,
                    });
                },
                dataType: "json"
            });
        },

        //            TODO: Show ALL usr in the spreadsheet.
        tpl: function() {
            var tpl = [
                '{@each res as user,index}',
                '   <tr data-id="${user.id}">',
                '       <td>${user.nick}</td>',
                '       <td>${user.time}</td>',
                '       <td>${user.content}</td>',
                '   </tr>',
                '{@/each}'
            ].join('\n');
            queryFunc.compiledTpl = juicer(tpl);
        }
    };


    $(function() {

        $(document).ready(function() {

            $('#reservation').daterangepicker({
                singleDatePicker: false
            }, function(start, end, label) {
                checkin = Date.parse(start.format('YYYY-MM-DD')) / 1000 - 28800;
                checkout = Date.parse(end.format('YYYY-MM-DD')) / 1000 - 28800;
                // console.log("开始：",checkin);
                // console.log("结束：",checkout)
            });
        });

    });

    function argsPass() {
        queryFunc.getUserHisData()
    }
    </script>
</aside>
