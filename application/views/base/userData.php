<!--/**-->
<!--* Created by PhpStorm.-->
<!--* User: Zander Wong-->
<!--* Date: 2016/4/28-->
<!--* Time: 10:37-->
<!--*/-->

<aside class="right-side">
    <!-- content header -->
    <section class="content-header">
        <h1>用户数据
            <small>欢迎来到GreeLink</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="./home"><i class="fa fa-dashboard"></i> 首页 </a></li>
            <li><a href="#">数据统计</a></li>
            <li class="active">用户数据</li>
        </ol>
    </section>
    <!-- main content -->
    <section class="content">
      <!-- DataTables -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- box-header -->
            <div class="box-header">
              <h3 class="box-title">Hover Data Table</h3>
              <div class="box-tools">
                <div class="input-group-btn" style="width: 450px;">
                    <form id="formTrans" action="userData/all" method="post">
                        <input id="all" type="button" class="btn btn-primary pull-right"
                               name="cycle_all" value="all" onclick="setAll()"/>
                        <input id="month" type="button" class="btn btn-primary pull-right"
                               name="cycle_month" value="monthly" onclick="setMonth()"/>
                        <input id="week" type="button" class="btn btn-primary pull-right"
                               name="cycle_week" value="weekly" onclick="setWeek()"/> 
                    </form>           
                </div>
              </div>              
            </div>
            <!-- /.box-header -->
            <!-- box-body -->
            <div class="box-body">
              <table id="udata" class="table table-bordered table-hover">
                <thead  style="font-size:12px">
                <tr>
                    <th>排 名</th>
                    <th>用 户</th>
                    <th>手 机</th>
                    <th>新 增</th>
                    <th>注册时间</th>
                    <th>部 门</th>
                    <th>签审数量</th>
                    <th>登陆次数</th>
                    <th>群组数量</th>
                    <th>消息条数</th>
                    <th>消息字节</th>
                </tr>
                </thead>
                <tbody>
                <!-- TODO: userData inject -->
                </tbody>
                <tfoot>
                <tr>
                    <th>排 名</th>
                    <th>用 户</th>
                    <th>手 机</th>
                    <th>新 增</th>
                    <th>注册时间</th>
                    <th>部 门</th>
                    <th>签审数量</th>
                    <th>登陆次数</th>
                    <th>群组数量</th>
                    <th>消息条数</th>
                    <th>消息字节</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>


<!-- DataTables -->
<script src="../ui/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../ui/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>

    window.onload = function () {
        setView();
    };

    var aimUrl = 'userData/all?boat=all';
    function setWeek() {
        aimUrl = 'userData/all?boat=weekly';
        setView();
    }
    function setMonth() {
        aimUrl = 'userData/all?boat=monthly';
        setView();
    }
    function setAll() {
        aimUrl = 'userData/all?boat=all';
        setView();
    }
    function setView() {
        var userData = {
            compiledTpl: null,
            getuserData: function (page) {
                if (!page) {
                    page = 0;
                }
                $.getJSON(aimUrl, {
                    start: page
                }, 
                function (data) {
                    userData.tpl();
                    var _tpl = userData.compiledTpl.render(data);
                    $("tbody").html(_tpl);
                    $('#udata').DataTable({
                      "paging": true,
                      "lengthChange": false,
                      "searching": false,
                      "ordering": true,
                      "info": true,
                      "autoWidth": false,
                      "retrieve":true,
                      "destroy":true,
                    });
                });
            },

            //            TODO: Show ALL usr in the spreadsheet.
            tpl: function () {
                var tpl = [
                    '{@each res as user,index}',
                    '   <tr data-id="${user.id}">',
                    '       <td>${index|addNum}</td>',
                    '       <td>${user.nick}</td>',
                    '       <td>${user.phone}</td>',
                    '       <td>${user.newGuy}</td>',
                    '       <td>${user.signup}</td>',
                    '       <td>${user.departName}</td>',
                    '       <td>${user.signum}</td>',
                    '       <td>${user.email}</td>',
                    '       <td>${user.gNum}</td>',
                    '       <td>${user.cnt}</td>',
                    '       <td>${user.siz}</td>',
                    '   </tr>',
                    '{@/each}'
                ].join('\n');
                userData.compiledTpl = juicer(tpl);
            }
        };

        $(function () {
            var addNum = function (data) {
                return parseInt(data) + 1;
            };

            juicer.register('addNum', addNum); //registry custom func

            userData.getuserData();
        })
    }
</script>

<style type="text/css">
    .input-group-btn input{
        width: 80px;
        margin-left: 10px;
        margin-bottom:10px;
    }
</style>
</aside>