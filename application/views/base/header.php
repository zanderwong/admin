<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>GreeLink后台管理系统</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GreeLink | Dashboard</title>
  <!-- 屏幕宽度自适应 -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link href="../ui/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../ui/ajax/libs/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="../ui/ajax/libs/ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../ui/plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../ui/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../ui/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../ui/plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../ui/plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../ui/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../ui/plugins/datatables/dataTables.bootstrap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../ui/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../ui/plugins/daterangepicker/daterangepicker.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="../ui/plugins/select2/select2.min.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../ui/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- simplemodal -->
  <link rel="stylesheet" href="../ui/dist/css/simplemodal.css" type="text/css"/>
  <!-- fullCalendar 2.2.5-->
  <link rel="stylesheet" href="../ui/plugins/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="../ui/plugins/fullcalendar/fullcalendar.print.css" media="print">  
  <!-- favicon -->
  <link rel="icon" href="../ui/ico/favicon.ico" type="image/x-icon" />
</head>


<body class="hold-transition skin-black sidebar-mini">
  <div class="wrapper">
    <!-- 页眉 -->
    <header class="main-header">
      <!-- Logo -->
      <a href="./home" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>G</b>ree</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Gree</b>Link</span>
      </a>


      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- 通知 -->
            <li class="dropdown notifications-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning">10</span>
              </a>
              <ul class="dropdown-menu">
                <li class="header">You have 10 notifications</li>
                <li>
                  <!-- inner menu: contains the actual data -->
                  <ul class="menu">
                    <li>
                      <a href="#">
                        <i class="fa fa-users text-aqua"></i> 5 new members joined today
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                        page and may cause design problems
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i class="fa fa-users text-red"></i> 5 new members joined
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i class="fa fa-user text-red"></i> You changed your username
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="footer"><a href="#">View all</a></li>
              </ul>
            </li>

            <!-- Task列表 -->
            <li class="dropdown tasks-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-flag-o"></i>
                <span class="label label-danger">9</span>
              </a>
              <ul class="dropdown-menu">
                <li class="header">You have 9 tasks</li>
                <li>
                  <!-- inner menu: contains the actual data -->
                  <ul class="menu">
                    <li><!-- Task item -->
                      <a href="#">
                        <h3>
                          Design some buttons
                          <small class="pull-right">20%</small>
                        </h3>
                        <div class="progress xs">
                          <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                            <span class="sr-only">20% Complete</span>
                          </div>
                        </div>
                      </a>
                    </li>
                    <!-- end task item -->
                    <li><!-- Task item -->
                      <a href="#">
                        <h3>
                          Create a nice theme
                          <small class="pull-right">40%</small>
                        </h3>
                        <div class="progress xs">
                          <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                            <span class="sr-only">40% Complete</span>
                          </div>
                        </div>
                      </a>
                    </li>
                    <!-- end task item -->
                    <li><!-- Task item -->
                      <a href="#">
                        <h3>
                          Some task I need to do
                          <small class="pull-right">60%</small>
                        </h3>
                        <div class="progress xs">
                          <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                            <span class="sr-only">60% Complete</span>
                          </div>
                        </div>
                      </a>
                    </li>
                    <!-- end task item -->
                    <li><!-- Task item -->
                      <a href="#">
                        <h3>
                          Make beautiful transitions
                          <small class="pull-right">80%</small>
                        </h3>
                        <div class="progress xs">
                          <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                            <span class="sr-only">80% Complete</span>
                          </div>
                        </div>
                      </a>
                    </li>
                    <!-- end task item -->
                  </ul>
                </li>
                <li class="footer">
                  <a href="#">View all tasks</a>
                </li>
              </ul>
            </li>

            <!-- 用户帐户 -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="../ui/dist/img/avatar04.png" class="user-image" alt="User Image">
                <span class="hidden-xs">admin</span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="../ui/dist/img/avatar04.png" class="img-circle" alt="User Image">

                  <p>
                    Alexander Pierce - Web Developer
                    <small>Member since Nov. 2012</small>
                  </p>
                </li>
                <!-- Menu Body -->
                <li class="user-body">
                  <div class="row">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </div>
                  <!-- /.row -->
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                  </div>
                  <div class="pull-right">
                    <a href="./auth/logout" class="btn btn-default btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>

    <!-- 左区域 -->
    <div id="navigation_collapse_bar">
      <aside class="">
        <aside class="main-sidebar">
          <!-- BEGIN 侧边栏 -->
          <section class="sidebar">
            <!-- 用户状态 -->
            <div class="user-panel">
              <div class="pull-left image">
                <img src="../ui/dist/img/avatar04.png" class="img-circle" alt="User Image">
              </div>
              <div class="pull-left info">
                <p>admin</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
              </div>
            </div>
            <!-- 导航 -->
            <ul class="sidebar-menu">
              <li class="header">MAIN NAVIGATION</li>

              <!-- Dashboard -->
              <li>
                <a href="./home">
                  <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
              </li>
              <!-- 用户管理 -->
              <li>
                <a href="./user">
                <i class="fa fa-user"></i> <span>用户管理</span>
                </a>
              </li>
              <!-- 群组管理 -->
              <li>
                <a href="./group">
                <i class="fa fa-group"></i> <span>群组管理</span>
                </a>
              </li>
              <!-- 组织架构 -->
              <li>
                <a href="./depart">
                <i class="fa fa-sitemap"></i> <span>组织架构</span>
                </a>
              </li>
              <!-- 系统设置 -->
              <li>
                <a href="./discovery">
                <i class="fa fa-cog"></i> <span>系统设置</span>
                </a>
              </li>
              <!-- 数据统计 -->
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-sort-amount-desc"></i>
                  <span>数据统计</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="./userData"><i class="fa fa-circle-o"></i> 用户</a></li>
                  <li><a href="./groupData"><i class="fa fa-circle-o"></i> 群组</a></li>
                  <li><a href="./midMgmtData"><i class="fa fa-circle-o"></i> 中干</a></li>
                </ul>
              </li>
              <!-- 历史查询 -->
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-laptop"></i>
                  <span>历史查询</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="./userHistory"><i class="fa fa-circle-o"></i> 用户</a></li>
                  <li><a href="./groupHistory"><i class="fa fa-circle-o"></i> 群组</a></li>
                </ul>
              </li>
              <!-- 表格DataTables实例 -->
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-table"></i> <span>Tables</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="./simpleTables"><i class="fa fa-circle-o"></i> Simple tables</a></li>
                  <li><a href="./dataTables"><i class="fa fa-circle-o"></i> Data tables</a></li>
                </ul>
              </li>
              <!-- 日历Canlendar实例 -->
              <li>
                <a href="./calendar">
                  <i class="fa fa-calendar"></i> <span>Calendar</span>
                  <span class="pull-right-container">
                    <small class="label pull-right bg-red">3</small>
                    <small class="label pull-right bg-blue">17</small>
                  </span>
                </a>
              </li>        	

              <li class="header">LABELS</li>
              <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
              <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
              <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
            </ul>
          </section>
          <!-- END 侧边栏 -->
        </aside>

        <!-- 内容跳转 —> home, user, group, depart... -->
      </aside>    
    </div>
  <!-- </div> -->
  


<!-- jQuery 2.2.3 -->
<script src="../ui/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../ui/jquery/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="../ui/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="../ui/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="../ui/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="../ui/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="../ui/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../ui/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="../ui/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="../ui/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="../ui/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="../ui/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../ui/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="../ui/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../ui/plugins/fastclick/fastclick.js"></script>
<!-- Juicer -->
<script src="../ui/dist/js/juicer.js" type="text/javascript"></script>
<!-- simple-modal -->
<script src="../ui/dist/js/simple-modal.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="../ui/dist/js/app.min.js"></script>
