<!--/**-->
<!--* Created by PhpStorm.-->
<!--* User: Zander Wong-->
<!--* Date: 2016/4/28-->
<!--* Time: 10:37-->
<!--*/-->
<!-- right side -->
<aside class="right-side">
    <!-- content header -->
    <section class="content-header">
        <h1>群组数据
            <small>欢迎来到GreeLink</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="./home"><i class="fa fa-dashboard"></i> 首页 </a></li>
            <li><a href="#">数据统计</a></li>
            <li class="active">群组数据</li>
        </ol>
    </section>
    <!-- main content -->
    <section class="content">
      <!-- DataTables -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- box-header -->
            <div class="box-header">
              <h3 class="box-title">Hover Data Table</h3>
              <div class="box-tools">
                <div class="input-group-btn" style="width: 450px;">
                    <form id="formTrans" action="groupData/all" method="post">
                        <input id="all" type="button" class="btn btn-primary pull-right"
                               name="cycle_all" value="all" onclick="setAll()"/>
                        <input id="month" type="button" class="btn btn-primary pull-right"
                               name="cycle_month" value="monthly" onclick="setMonth()"/>
                        <input id="week" type="button" class="btn btn-primary pull-right"
                               name="cycle_week" value="weekly" onclick="setWeek()"/>
                    </form>                
                </div>
              </div>              
            </div>
            <!-- /.box-header -->
            <!-- box-body -->
            <div class="box-body">
              <table id="gdata" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>排名</th>
                    <th>群组</th>
                    <th>新增</th>
                    <th>建立时间</th>
                    <th>群 主</th>
                    <th>成员数量</th>
                    <th>消息条数</th>
                    <th>消息字节</th>
                </tr>
                </thead>
                <tbody>
                <!-- TODO: groupData inject -->
                </tbody>
                <tfoot>
                <tr>
                    <th>排名</th>
                    <th>群组</th>
                    <th>新增</th>
                    <th>建立时间</th>
                    <th>群 主</th>
                    <th>成员数量</th>
                    <th>消息条数</th>
                    <th>消息字节</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>



<!-- DataTables -->
<script src="../ui/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../ui/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>

    window.onload = function () {
        setView();
    };
    var aimUrl = 'groupData/all?boat=all';
    function setWeek() {
        aimUrl = 'groupData/all?boat=weekly';
        setView();
    }
    function setMonth() {
        aimUrl = 'groupData/all?boat=monthly';
        setView();
    }
    function setAll() {
        aimUrl = 'groupData/all?boat=all';
        setView();
    }
    function setView() {
        var GroupData = {
            compiledTpl: null,
            getGroupData: function (page) {
                if (!page) {
                    page = 0;
                }
                $.getJSON(aimUrl, {
                    start: page
                }, 
                function (data) {
                    GroupData.tpl();
                    var _tpl = GroupData.compiledTpl.render(data);
                    $("tbody").html(_tpl);
                    $('#gdata').DataTable({
                      "paging": true,
                      "lengthChange": false,
                      "searching": false,
                      "ordering": true,
                      "info": true,
                      "autoWidth": false
                    });
                });
            },

            // TODO: Show ALL usr in the spreadsheet.
            tpl: function () {
                var tpl = [
                    '{@each gres as user}',
                    '   <tr data-id="${user.id}">',
                    '       <td>${user.ranking}</td>',
                    '       <td>${user.GroupName}</td>',
                    '       <td>${user.newGroup}</td>',
                    '       <td>${user.CreationTime}</td>',
                    '       <td>${user.Creator}</td>',
                    '       <td>${user.PCU}</td>',
                    '       <td>${user.MsgCount}</td>',
                    '       <td>${user.MsgSize}</td>',
                    '   </tr>',
                    '{@/each}'
                ].join('\n');
                GroupData.compiledTpl = juicer(tpl);
            }
        };

        $(function () {
            GroupData.getGroupData();
        })
    }
</script>

<style type="text/css">
    .input-group-btn input{
        width: 80px;
        margin-left: 10px;
        margin-bottom:10px;
    }
</style>


</aside>