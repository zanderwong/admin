<!--/**-->
<!-- * Created by PhpStorm.-->
<!-- * User: Zander Wong-->
<!-- * Date: 2016/7/4-->
<!-- * Time: 10:50-->
<!-- */-->

<head>
    <meta charset="utf-8">
    <meta name="viewport" http-equiv="content-type" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../ui/css/jquery.Jcrop.css" type="text/css"/>
    <link rel="stylesheet" href="../ui/bootstrap/css/bootstrap.min.css" media="screen">
    <link rel="stylesheet" href="../ui/css/bootstrap-datetimepicker.min.css" media="screen">

    <script src="../ui/js/jquery.Jcrop.js"></script>
    <script type="text/javascript" src="../ui/jquery/jquery-1.8.3.min.js" charset="UTF-8"></script>
    <script type="text/javascript" src="../ui/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../ui/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
</head>

<aside class="right-side">

    <section class="content-header">
        <h1>群组历史
            <small>欢迎来到GreeLink</small>
        </h1>

        <ol class="breadcrumb">
            <li><a href="./home"><i class="fa fa-dashboard"></i> 首页 </a></li>
            <li class="active">群组历史</li>
        </ol>
    </section>

    <section class="content">

        <table class="table table-bordered" style="word-break: keep-all;white-space:nowrap;">
            <thead>
            <tr>
                <div class="container">
                    <form action="" class="form-horizontal">
                        <!-- TODO:add groupname input field here -->
                        <td width="10%">
                            <div class="input-group input-group-sm span6" style="margin:auto;width: auto">
                                <label><strong>群组</strong></label>
                                <input id="gname" class="span2" placeholder="GroupName"
                                       aria-describedby="sizing-addon3" type="text"
                                       style="height: auto;margin:0px auto;width: auto">
                            </div>
                        </td>
                        <!-- TODO:add username input field here -->
                        <td width="6%">
                            <div class="input-group input-group-sm span6" style="margin:auto;width: auto">
                                <label><strong>发送人</strong></label>
                                <input id="uname" class="span2" placeholder="Username"
                                       aria-describedby="sizing-addon3" type="text"
                                       style="height: auto;margin:0px auto;width: 100%">
                            </div>
                        </td>
                        <!-- TODO:add datePicker field here -->
                        <td width="30%">
                            <div class="" style="margin-left:0px;margin-right:0px">
                                <label><strong>会话时间</strong></label>
                                <span style="margin-left:0px">From: </span>
                                <input class="span2" id="dpd1" type="text" placeholder="Start Date"
                                       style="height: auto;width: auto">
                                <span style="margin-left:10px">To: </span>
                                <input class="span2" id="dpd2" type="text" placeholder="End Date"
                                       style="height: auto;width: auto">
                            </div>
                        </td>
                        <!-- TODO:add submit button here -->
                        <td>
                            <div class="btn-group" role="group" aria-label="...">
                                <label><strong>会话历史</strong></label>
                                <button type="button" class="btn btn-default" onclick="argsPass()">显示内容</button>
                            </div>
                        </td>
                    </form>
                </div>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </section>


    <script type="text/javascript">

        var queryFunc = {
            compiledTpl: null,
            getGroupHisData: function () {

                var gname = $("#gname").val();
                var uname = $("#uname").val();
                var checkin = $("#dpd1").val();
                var checkout = $("#dpd2").val();

                checkin = (new Date(checkin)).getTime();
                checkout = (new Date(checkout)).getTime();

                console.log(checkin);
                console.log(checkout);


                $.ajax({
                    url: "./groupHistory/all",
                    type: "get",
                    data: {
                        "gname": gname,
                        "uname": uname,
                        "checkin": checkin,
                        "checkout": checkout
                    },
                    success: function (data) {
                        queryFunc.tpl();
                        var _tpl = queryFunc.compiledTpl.render(data);
                        $("tbody").html(_tpl);

                    },
                    dataType: "json"
                });
            },

            //            TODO: Show ALL groupHistory in the spreadsheet.
            tpl: function () {
                var tpl = [
                    '{@each res as user,index}',
                    '   <tr data-id="${user.id}">',
                    '       <td>${user.gName}</td>',
                    '       <td>${user.uName}</td>',
                    '       <td>${user.time}</td>',
                    '       <td>${user.content}</td>',
                    '   </tr>',
                    '{@/each}'
                ].join('\n');
                queryFunc.compiledTpl = juicer(tpl);
            }
        };


        $(function () {

            var nowTemp = new Date();
            var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);


            var checkin = $("#dpd1").datetimepicker({
                format: 'yyyy-mm-dd',
                todayBtn: true,
                autoclose: true,
                minView: 'month'
            }).on('changeDate', function (ev) {
                if (ev.date.valueOf() > checkout.date.valueOf()) {
                    var newDate = new Date(ev.date);
                    newDate.setDate(newDate.getDate() + 1);
                    checkout.setDate(newDate);
                }
                $('#dpd2')[0].focus();
            }).data("datetimepicker");

            var checkout = $("#dpd2").datetimepicker({
                format: 'yyyy-mm-dd',
                todayBtn: true,
                autoclose: true,
                minView: 'month'
            }).data("datetimepicker");
        });

        function argsPass() {
            queryFunc.getGroupHisData()
        }
    </script>


    <style>
        .add_user_div .div_20 {
            margin-top: 20px;
        }

        .radio span {
            display: inline-block;
            width: 50px;
        }

        .add_user_div div.radio {
            margin: 0;
        }

        .add_user_div input, .radio, .add_user_div select {
            width: 300px;
            float: left;
        }

        .radio input {
            width: 20px;
        }
    </style>
</aside>